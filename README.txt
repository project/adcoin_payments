Allow your customers to pay or donate through AdCoin.

!!! Important Note: !!!
Receiving AdCoin payments is not possible through a localhost without it being
an externally accessible domain.