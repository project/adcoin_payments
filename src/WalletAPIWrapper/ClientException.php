<?php

namespace Drupal\adcoin_payments\WalletAPIWrapper;

/**
 * Class ClientException
 */
class ClientException extends \Exception
{
}
