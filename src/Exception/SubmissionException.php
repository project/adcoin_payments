<?php
/**
 * @author appels
 */
namespace Drupal\adcoin_payments\Exception;
class SubmissionException extends \Exception {}