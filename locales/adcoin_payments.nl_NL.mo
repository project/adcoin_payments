��    !      $  /   ,      �     �     �  #   �          *  -   =     k     w  
   ~  	   �  
   �  
   �     �     �     �     �     �     �     
          .     =     Q     X     l  Y   x     �     �     �  �   �  
   �  	   �  �  �     �
     �
  (   �
     �
     �
  <   �
  
   "  	   -     7     G  
   P     [     j     }     �     �     �      �     �     �               '     .     C  K   O  	   �     �     �  �   �     O     X                                  	      
   !                                                                                                                    API Key Amount Amount has to be a positive number. Amount in ACC An error occurred. Are you sure you want to delete this payment? Button text Cancel Cancel URL Completed Created At Delete it! Enable email field Enable name field Field Name No payments found. Only do this if you are sure! Paid & Confirmed Paid (Unconfirmed) Payment Status Payment description Status Succesfully deleted Success URL Thank you for choosing to pay through AdCoin. Please wait for your order to be confirmed. Unpaid Update Status Value Your AdCoin Wallet API key.<br>You can find the key in your <a href="https://wallet.getadcoin.com">AdCoin Web Wallet</a> under your name > "API Key". Your email Your name Project-Id-Version: AdCoin Payments
POT-Creation-Date: 2018-03-27 15:13+0200
PO-Revision-Date: 2018-03-27 15:16+0200
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ../src
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: t
X-Poedit-SearchPath-0: Controller/ListController.php
X-Poedit-SearchPath-1: Controller/ReturnController.php
X-Poedit-SearchPath-2: Controller/ViewPaymentController.php
X-Poedit-SearchPath-3: Controller/WebhookController.php
X-Poedit-SearchPath-4: Exception/DatabaseException.php
X-Poedit-SearchPath-5: Exception/ExceptionHandler.php
X-Poedit-SearchPath-6: Exception/ReturnException.php
X-Poedit-SearchPath-7: Exception/SubmissionException.php
X-Poedit-SearchPath-8: Exception/WebhookException.php
X-Poedit-SearchPath-9: Form/ConfigurationForm.php
X-Poedit-SearchPath-10: Form/DeleteForm.php
X-Poedit-SearchPath-11: Form/PaymentForm.php
X-Poedit-SearchPath-12: Form/ViewPaymentForm.php
X-Poedit-SearchPath-13: Model/PageList.php
X-Poedit-SearchPath-14: Model/PaymentStorage.php
X-Poedit-SearchPath-15: Model/Settings.php
X-Poedit-SearchPath-16: Plugin/Block/PaymentFormBlock.php
 API Sleutel Aantal Het aantal moet een positief getal zijn. Aantal in ACC Er is een fout opgetreden. Weet u zeker dat u deze betaling permanent wilt verwijderen? Knop tekst Annuleren Annulerings URL Afgerond Gemaakt Op Verwijder het! Gebruik email veld Gebruik naam veld Veld Naam Geen betalingen beschikbaar. Doe dit alleen als u zeker bent! Betaald & Bevestigd Betaald (Onbevestigd) Betalingsstatus Betalingsbeschrijving Status Succesvol verwijdert Success URL Bedankt voor uw keuze om met AdCoin te betalen. Uw order wordt goedgekeurd. Onbetaald Status Bijwerken Waarde Uw AdCoin Wallet API sleutel.<br>U kunt deze vinden in uw <a href="https://wallet.getadcoin.com">AdCoin Web Wallet</a> onder uw naam > "API Key". Uw email Uw naam 